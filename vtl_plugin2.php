<?php 

/*
Plugin Name: Metadatos de video para mis posts
Plugin URI: http://www.davidfraj.com/
Description: mi segundo plugin
Author: verillo
Author URI: 
License: Licencia
*/

// mostramos una label para mi post
add_action('add_meta_boxes', 'vtl_add_metabox');

add_action('save_post', 'vtl_save_metabox');

// register widgets QUÉ FUNCION VA A REGISTRAR EL WIDGET
add_action('widgets_init', 'vtl_widget_init');

// vtl_youtube es el nombre de la metakey

function vtl_add_metabox(){
	add_meta_box('vtl_youtube', 'Youtube video Link', 'vtl_youtube_handler', 'post');
}

function vtl_youtube_handler() {
    $value = get_post_custom($post->ID);
    $youtube_link = esc_attr($value['vtl_youtube'][0]);
    $youtube_link2 = esc_attr($value['vtl_youtube2'][0]);
    echo '<label for="vtl_youtube">YouTube Video Link</label><input type="text" id="vtl_youtube" name="vtl_youtube" value="'.$youtube_link.'" /><br>';
    echo '<label for="vtl_youtube2">Titulo Video Link</label><input type="text" id="vtl_youtube" name="vtl_youtube2" value="'.$youtube_link2.'" />';
}

/**
 * save metadata
 */
function vtl_save_metabox($post_id) {
    //don't save metadata if it's autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return; 
    }    
    
    //check if user can edit post
    if( !current_user_can( 'edit_post' ) ) {
        return;  
    }


    
    if( isset($_POST['vtl_youtube'] )) {
        update_post_meta($post_id, 'vtl_youtube', $_POST['vtl_youtube']);
    }

    if( isset($_POST['vtl_youtube2'] )) {
        update_post_meta($post_id, 'vtl_youtube2', $_POST['vtl_youtube2']);
    }
}

// funcion para registrar el widget (añadir su clase), llamada por add_action en la parte superior

function vtl_widget_init(){

	register_widget(Vtl_Widget);
}

//Tenemos que crear la nueva clase
/**
 * widget class
 */
class Vtl_Widget extends WP_Widget {

	// las funciones estan sobreescritas
    function Vtl_Widget() {
        $widget_options = array(
            'classname' => 'vtl_class', //CSS
            'description' => 'Show a YouTube Video from post metadata'
        );
        
        $this->WP_Widget('vtl_id', 'YouTube Video', $widget_options);
    }
    
    /**
     * show widget form in Appearence / Widgets
     */
    function form($instance) {
        $defaults = array('title' => 'Video');
        $instance = wp_parse_args( (array) $instance, $defaults);
        
        $title = esc_attr($instance['title']);
        
        echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
    }
    
    /**
     * save widget form
     */
    function update($new_instance, $old_instance) {
        
        $instance = $old_instance;        
        $instance['title'] = strip_tags($new_instance['title']);        
        return $instance;
    }
    
    /**
     * show widget in post / page
     */
    function widget($args, $instance) {
        extract( $args );        
        $title = apply_filters('widget_title', $instance['title']);
        
        //show only if single post
        if(is_single()) {
            echo $before_widget;
            echo $before_title.$title.$after_title;
            
            //get post metadata
            $vtl_youtube = get_post_meta(get_the_ID(), 'vtl_youtube', true);
            $vtl_youtube2 = get_post_meta(get_the_ID(), 'vtl_youtube2', true);
            
            //print widget content
            echo '<iframe width="200" height="200" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/'.($vtl_youtube).'"></iframe>';       
            
            echo $vtl_youtube2;
            echo $after_widget;
        }
    }

}

 ?>